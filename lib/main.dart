import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String data = 'belum ada data';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('HTTP DELETE'),
        actions: [
          IconButton(
              onPressed: () async {
                var response =
                    await http.get(Uri.parse('https://reqres.in/api/users/2'));
                Map<String, dynamic> myhttp = json.decode(response.body);
                setState(() {
                  data =
                      'nama : ${myhttp['data']['first_name']} ${myhttp['data']['last_name']}';
                });
              },
              icon: Icon(Icons.get_app))
        ],
      ),
      body: ListView(
        children: [
          SizedBox(
            height: 35,
          ),
          Text(data),
          SizedBox(
            height: 35,
          ),
          ElevatedButton(
              onPressed: () async {
                var response = await http
                    .delete(Uri.parse('https://reqres.in/api/users/2'));
                setState(() {
                  if (response.statusCode == 204) {
                    setState(() {
                      data = 'DATA BERHASIL DIHAPUS';
                    });
                  }
                });
              },
              child: Text('delete'))
        ],
      ),
    );
  }
}
